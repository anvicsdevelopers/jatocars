//
//  TransformAnimatics.swift
//  PokeScrum
//
//  Created by Nikita Arkhipov on 19.09.15.
//  Copyright © 2015 Anvics. All rights reserved.
//

import Foundation
import UIKit

class IdentityTransformAnimator: AnimationSettingsHolder, AnimaticsViewChangesPerformer {
   typealias TargetType = UIView
   typealias ValueType = ()
   var a: Int = 7
   let value: ValueType
   
   required init(_ v: ValueType){ value = v }
   func _updateForTarget(t: TargetType){ t.transform = CGAffineTransformIdentity }
   func _currentValue(target: TargetType) -> ValueType { return () }
}

class ViewTransformAnimatics: AnimationSettingsHolder, AnimaticsViewChangesPerformer {
   typealias TargetType = UIView
   typealias ValueType = CGAffineTransform
   
   let value: ValueType
   
   required init(_ v: ValueType){ value = v }
   
   func _updateForTarget(t: TargetType){ fatalError() }
   func _currentValue(target: TargetType) -> ValueType { return target.transform }
}

class TransformAnimator: ViewTransformAnimatics{
   override func _updateForTarget(t: TargetType) { t.transform = value }
}

class AdditiveTransformAnimator: ViewTransformAnimatics {
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformConcat(t.transform, value) }
}

class ScaleAnimator: ViewFloatAnimatics {
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformMakeScale(value, value) }
   override func _currentValue(target: TargetType) -> ValueType {
      let t = target.transform
      return sqrt(t.a*t.a + t.c*t.c)
   }
}

class ScaleXYAnimator: AnimationSettingsHolder, AnimaticsViewChangesPerformer{
   typealias TargetType = UIView
   typealias ValueType = (CGFloat, CGFloat)
   let value: ValueType
   
   required init(_ v: ValueType){ value = v }
   func _updateForTarget(t: TargetType){ t.transform = CGAffineTransformMakeScale(value.0, value.1) }
   func _currentValue(target: TargetType) -> ValueType {
      let t = target.transform
      let sx = sqrt(t.a*t.a + t.c*t.c)
      let sy = sqrt(t.b*t.b + t.d*t.d)
      return (sx, sy)
   }
}

class AdditiveScaleAnimator: ViewFloatAnimatics{
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformConcat(t.transform, CGAffineTransformMakeScale(value, value)) }
   override func _currentValue(target: TargetType) -> ValueType {
      let t = target.transform
      return sqrt(t.a*t.a + t.c*t.c)
   }
}

class RotateAnimator: ViewFloatAnimatics {
   convenience init(_ v: Double) { self.init(CGFloat(v)) }
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformMakeRotation(value) }
   override func _currentValue(target: TargetType) -> ValueType { return atan2(target.transform.b, target.transform.a) }
}

class AdditiveRotateAnimator: ViewFloatAnimatics {
   convenience init(_ v: Double) { self.init(CGFloat(v)) }
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformConcat(t.transform, CGAffineTransformMakeRotation(value)) }
   override func _currentValue(target: TargetType) -> ValueType { return atan2(target.transform.b, target.transform.a) }
}

class XTranslateAnimator: ViewFloatAnimatics{
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformMakeTranslation(value, 0) }
   override func _currentValue(target: TargetType) -> ValueType { return target.transform.tx }
}

class YTranslateAnimator: ViewFloatAnimatics{
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformMakeTranslation(0, value) }
   override func _currentValue(target: TargetType) -> ValueType { return target.transform.ty }
}

class TranslateAnimator: ViewPointAnimatics{
   override func _updateForTarget(t: TargetType) { t.transform = CGAffineTransformMakeTranslation(value.x, value.y) }
   override func _currentValue(target: TargetType) -> ValueType { return CGPoint(x: target.transform.tx, y: target.transform.ty) }
}
