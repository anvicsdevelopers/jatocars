//
//  AnimaticsFoundation.swift
//  AnimationFramework
//
//  Created by Nikita Arkhipov on 14.09.15.
//  Copyright © 2015 Anvics. All rights reserved.
//

import Foundation

protocol AnimaticsTargetWaiter: AnimaticsSettingsSetter{
   associatedtype TargetType: AnyObject
   func to(t: TargetType) -> AnimaticsReady
}

protocol Animatics: AnimaticsSettingsHolder, AnimaticsTargetWaiter{
   associatedtype ValueType
   
   var value: ValueType { get }
   
   init(_ v: ValueType)
   
   func _animateWithTarget(t: TargetType, completion: AnimaticsCompletionBlock?)
   func _performWithoutAnimationToTarget(t: TargetType)
   func _cancelAnimation(t: TargetType, shouldShowFinalState: Bool)
   func _currentValue(target: TargetType) -> ValueType
}

extension Animatics{
   func to(target: TargetType) -> AnimaticsReady{ return AnimationReady(animator: self, target: target) }
   
   func animaticsReadyCreator() -> TargetType -> AnimaticsReady{ return to }
}

protocol AnimaticsReady: AnimaticsSettingsSetter {
   func animateWithCompletion(completion: AnimaticsCompletionBlock?)
   func performWithoutAnimation()
   func cancelAnimation(shouldShowFinalState: Bool)
   func reversedAnimation() -> AnimaticsReady
   func getDuration() -> NSTimeInterval
}

extension AnimaticsReady{
   func animate(){ animateWithCompletion(nil) }
   func animateWithCompletion(completion: EmptyBlock?){ animateWithCompletion(emptyBlock(completion)) }
}

final class AnimationReady<T: Animatics>: AnimaticsReady, AnimaticsSettingsSettersWrapper {
   let animator: T
   weak var target: T.TargetType?
   let reverseAnimator: T
   
   init(animator: T, target: T.TargetType){
      self.animator = animator
      self.target = target
      self.reverseAnimator = T(animator._currentValue(target))
   }
   
   func animateWithCompletion(completion: AnimaticsCompletionBlock? = nil){
      if let target = target{
         animator._animateWithTarget(target, completion: completion)
      }
   }
   
   func performWithoutAnimation(){
      if let target = target{
         animator._performWithoutAnimationToTarget(target)
      }
   }
   
   func cancelAnimation(shouldShowFinalState: Bool) {
      if let target = target{
         animator._cancelAnimation(target, shouldShowFinalState: shouldShowFinalState)
      }
   }
   
   func reversedAnimation() -> AnimaticsReady {
      if let target = target{
         return reverseAnimator.copySettingsFrom(animator).to(target)
      }else{
         return EmptyAnimator()
      }
   }

   func getSettingsSetters() -> [AnimaticsSettingsSetter]{ return [animator] }
   func getDuration() -> NSTimeInterval{ return animator._duration + animator._delay }
}

infix operator |-> { associativity left }
infix operator ~> {}
infix operator ~?> {}
func ~><T: AnimaticsTargetWaiter>(a: T, t: T.TargetType){
   a.to(t).animate()
}

func ~><T: AnimaticsTargetWaiter>(a: T, targets: [T.TargetType]){
   for t in targets{ a.to(t).animate() }
}


func ~?><T: AnimaticsTargetWaiter>(a: T, t: T.TargetType?){
   if let t = t{ a.to(t).animate() }
}


func Animatics_GCD_After(seconds: Double, block: () -> ()){
   let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(seconds * Double(NSEC_PER_SEC)))
   dispatch_after(delayTime, dispatch_get_main_queue()) {
      block()
   }
}

typealias EmptyBlock = () -> ()
func emptyBlock<T>(block: EmptyBlock) -> T -> (){
   return { _ in block() }
}

func emptyBlock<T>(block: EmptyBlock?) -> T -> (){
   return { _ in block?() }
}


func fillBlock<T>(block: T -> (), value: T) -> EmptyBlock{
   return { block(value) }
}

func fillBlock<T>(block: (T -> ())?, value: T) -> EmptyBlock{
   return { block?(value) }
}

