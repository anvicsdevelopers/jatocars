//
//  AnimationPerformers.swift
//  PokeScrum
//
//  Created by Nikita Arkhipov on 20.09.15.
//  Copyright © 2015 Anvics. All rights reserved.
//

import Foundation
import UIKit

protocol AnimaticsViewChangesPerformer: Animatics{
   func _updateForTarget(t: TargetType)
}

extension AnimaticsViewChangesPerformer{
   func _animateWithTarget(t: TargetType, completion: AnimaticsCompletionBlock?){
      ViewAnimationPerformer.sharedInstance.animate(self, target: t, completion: completion)
   }
   func _performWithoutAnimationToTarget(t: TargetType){
      _updateForTarget(t)
   }
}

extension AnimaticsViewChangesPerformer where TargetType: UIView{
   func _cancelAnimation(t: TargetType, shouldShowFinalState: Bool) {
      t.layer.removeAllAnimations()
      if shouldShowFinalState { _updateForTarget(t) }
   }
}

final class ViewAnimationPerformer{
   static let sharedInstance = ViewAnimationPerformer()

   func animate<T: AnimaticsViewChangesPerformer>(animator: T, target: T.TargetType, completion: AnimaticsCompletionBlock?){
      if animator._isSpring{
         UIView.animateWithDuration(animator._duration, delay: animator._delay, usingSpringWithDamping: animator._springDumping, initialSpringVelocity: animator._springVelocity, options: animator._animationOptions, animations: {
            animator._updateForTarget(target)
            }, completion: completion)
      }else{
         UIView.animateWithDuration(animator._duration, delay: animator._delay, options: animator._animationOptions, animations: {
            animator._updateForTarget(target)
         }, completion: completion)
      }
   }
}

protocol AnimaticsLayerChangesPerformer: Animatics{
   func _animationKeyPath() -> String
   func _newValue() -> AnyObject
   func _layerForTarget(target: TargetType) -> CALayer
}

extension AnimaticsLayerChangesPerformer{
   func _newValue() -> AnyObject { return value as! AnyObject }
   func _animateWithTarget(t: TargetType, completion: AnimaticsCompletionBlock?){
      LayerAnimationPerformer.sharedInstance.animate(self, target: _layerForTarget(t), completion: completion)
   }
   func _performWithoutAnimationToTarget(t: TargetType) {
      _layerForTarget(t).setValue(_newValue(), forKeyPath: _animationKeyPath())
   }
   
   func _cancelAnimation(t: TargetType, shouldShowFinalState: Bool) {
      _layerForTarget(t).removeAllAnimations()
      if shouldShowFinalState { _layerForTarget(t).setValue(_newValue(), forKeyPath: _animationKeyPath()) }
   }
   
   func _currentValue(target: TargetType) -> ValueType {
      return _layerForTarget(target).valueForKeyPath(_animationKeyPath()) as! ValueType
   }
}

extension AnimaticsLayerChangesPerformer where TargetType: UIView{
   func _layerForTarget(target: TargetType) -> CALayer{ return target.layer }
}

extension AnimaticsLayerChangesPerformer where TargetType: CALayer{
   func _layerForTarget(target: TargetType) -> CALayer{ return target }
}

final class LayerAnimationPerformer{
   class var sharedInstance : LayerAnimationPerformer {
      struct Static {
         static let instance = LayerAnimationPerformer()
      }
      return Static.instance
   }
   
   func animate<T: AnimaticsLayerChangesPerformer, U: CALayer>(animator: T, target: U, completion: AnimaticsCompletionBlock?){
      let key = animator._animationKeyPath()
      
      func createAnimation() -> CABasicAnimation{
         if animator._isSpring{
//            if #available(iOS 9, *){
               let animation = CASpringAnimation(keyPath: key)
               animation.damping = animator._springDumping * 10
               animation.initialVelocity = animator._springVelocity
               animation.duration = animation.settlingDuration
               return animation
//            }
         }
         let animation = CABasicAnimation(keyPath: key)
         return animation
      }

      let fromValue = target.valueForKeyPath(key)
      target.setValue(animator._newValue(), forKeyPath: key)
      target.removeAnimationForKey(key)
      let animation = CABasicAnimation(keyPath: key)
      
      animation.duration = animator._duration
      animation.beginTime = animator._delay
      animation.fromValue = fromValue
      target.addAnimation(animation, forKey: key)
      Animatics_GCD_After(animator._duration + animator._delay) { completion?(true) }
   }
}
