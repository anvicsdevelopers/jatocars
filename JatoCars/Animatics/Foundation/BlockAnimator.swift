//
//  BlockAnimator.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 12.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class BlockAnimator: AnimationSettingsHolder, AnimaticsReady{
   typealias AnimationType = (AnimaticsSettingsHolder, Bool, AnimaticsCompletionBlock?) -> ()
   typealias DurationDeterminer = AnimaticsSettingsHolder -> NSTimeInterval
   typealias AnimationCreator = AnimaticsSettingsHolder -> AnimaticsReady
   
   let animation: AnimationType
   let durationDeterminer: DurationDeterminer?
   let reversedAnimationCreator: AnimationCreator?
   
   init(animation: AnimationType, durationDeterminer: DurationDeterminer? = nil, reversedAnimationCreator: AnimationCreator? = nil){
      self.animation = animation
      self.durationDeterminer = durationDeterminer
      self.reversedAnimationCreator = reversedAnimationCreator
   }
   
   func animateWithCompletion(completion: AnimaticsCompletionBlock?){
      animation(self, true, completion)
   }
   
   func performWithoutAnimation(){
      animation(self, false, nil)
   }
   
   func cancelAnimation(shouldShowFinalState: Bool){
      //TODO: implement
   }
   
   func reversedAnimation() -> AnimaticsReady{
      return reversedAnimationCreator?(self) ?? EmptyAnimator()
   }
   
   func getDuration() -> NSTimeInterval{
      return durationDeterminer?(self) ?? 0
   }
}