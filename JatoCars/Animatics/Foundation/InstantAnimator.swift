//
//  InstantAnimator.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 22.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import UIKit

class InstantAnimator: AnimationSettingsHolder, AnimaticsReady{
   
   private let block: () -> ()
   
   init(block: () -> ()) {
      self.block = block
   }
   
   func animateWithCompletion(completion: AnimaticsCompletionBlock?){
      GCD_After(_delay, block: block)
      completion?(true)
   }
   
   func performWithoutAnimation(){
      block()
   }
   
   func cancelAnimation(shouldShowFinalState: Bool){
   }
   
   func reversedAnimation() -> AnimaticsReady{
      return EmptyAnimator()
   }
   
   func getDuration() -> NSTimeInterval{
      return 0
   }
}

extension AnimaticsReady{
   func instant() -> AnimaticsReady{ return InstantAnimator(block: performWithoutAnimation) }
}