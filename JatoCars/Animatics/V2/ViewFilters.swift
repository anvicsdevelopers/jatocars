//
//  ViewFilters.swift
//  ProjectFluid
//
//  Created by Nikita Arkhipov on 15.03.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
   func animatics_snapshotImage() -> UIImage{
      UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0);
      let view = self
      view.layoutIfNeeded()
      if !view.drawViewHierarchyInRect(bounds, afterScreenUpdates: false) {
         view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
      }
      let image = UIGraphicsGetImageFromCurrentImageContext();
      UIGraphicsEndImageContext();
      return image
   }

}

class ViewFilterAndBackAnimator: AnimationSettingsHolder, Animatics {
   typealias TargetType = UIView
   typealias ValueType = Filter

   let value: ValueType
   
   private var tmpImageView: UIImageView!
   
   required init(_ v: ValueType){ value = v }

   func _animateWithTarget(t: TargetType, completion: AnimaticsCompletionBlock?){
      let image = t.animatics_snapshotImage()
      let ciimage = CIImage(image: image)!
      tmpImageView = UIImageView(image: UIImage(CIImage: value(ciimage)))
      tmpImageView.frame = t.bounds
      tmpImageView.alpha = 0
      t.addSubview(tmpImageView)
      
      (AlphaAnimator(1).copySettingsFrom(self) |-> AlphaAnimator(0).copySettingsFrom(self)).duration(_duration/2).to(tmpImageView).animateWithCompletion { (completed: Bool) -> Void in
         self.tmpImageView.removeFromSuperview()
         completion?(completed)
      }
   }
   
   func _performWithoutAnimationToTarget(t: TargetType) { }
   
   func _cancelAnimation(t: TargetType, shouldShowFinalState: Bool) {
      tmpImageView?.removeFromSuperview()
   }
   
   func _currentValue(target: TargetType) -> ValueType { return value }
}