//
//  BondExtensions.swift
//  BlackStar
//
//  Created by Nikita Arkhipov on 18.02.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation
import UIKit

typealias Signal = EventProducer<()>

extension UIButton{
   var bnd_titleColor: Observable<UIColor?>{
      return bnd_associatedObservableForValueForKey("titleColor", initial: titleColorForState(UIControlState.Normal)) { [weak self] color in
         self?.setTitleColor(color, forState: UIControlState.Normal)
      }
   }
   
   public var bnd_image: Observable<UIImage?> {
      return bnd_associatedObservableForValueForKey("image", initial: self.imageForState(.Normal)) { [weak self] image in
         self?.setImage(image, forState: .Normal)
      }
   }
   
}

extension EventProducer{
   func listen(listener: Event -> Void) -> EventProducer<Event>{
      return self.map { v in
         listener(v)
         return v
      }
   }
   
   func rewrite<T>(value: T) -> EventProducer<T>{
      return map { _ in value }
   }
   
   func signal() -> Signal{
      return map { _ in () }
   }
   
   func invert(o: Observable<Bool>) -> DisposableType{
      return observe { [weak o] _ in o?.value = !(o?.value ?? true) }
   }
}

extension EventProducer where Event: Equatable{
   func waitFor(value: Event) -> Signal{
      return filter { $0 == value }.signal()
   }
}

extension EventProducer where Event: BooleanType{
   func inverse() -> EventProducer<Bool>{
      return map { !$0.boolValue }
   }
}

extension EventProducer where Event: CustomStringConvertible{
   func toImage() -> EventProducer<UIImage?>{
      return map { UIImage(named: $0.description) }
   }
   
   func log(){
      observe { print($0.description) }
   }
}


extension UIView{
   public var bnd_x: Observable<CGFloat> {
      return bnd_associatedObservableForValueForKey("x", initial: self.frame.origin.x) { [weak self] x in
         self?.frame.origin.x = x
      }
   }
   
   public var bnd_centerX: Observable<CGFloat> {
      return bnd_associatedObservableForValueForKey("centerX", initial: self.center.x) { [weak self] x in
         self?.center.x = x
      }
   }
   
   public var bnd_width: Observable<CGFloat> {
      return bnd_associatedObservableForValueForKey("width", initial: self.frame.size.width) { [weak self] width in
         if width.isNaN || width.isInfinite || width < 0 { return }
         self?.frame.size.width = width
      }
   }
   
   public var bnd_frame: Observable<CGRect> {
      return bnd_associatedObservableForValueForKey("frame", initial: self.frame) { [weak self] frame in
         self?.frame = frame
      }
   }
}

//EventProducer<ObservableArrayEvent<Array<ElementType>>>, ObservableArrayType
//extension EventProducer{
//   func bindTo<T: ObservableArrayType where Event = [T.ElementType]>(array: ObservableArray<T>){
//      
//   }
//}
