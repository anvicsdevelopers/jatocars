//
//  CarsViewController.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 29.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import UIKit
import MessageUI

class CarsViewController: UIViewController {

   @IBOutlet weak var tableView: UITableView!
   
   var cars = ObservableArray<JATOCar>()
   
   @IBAction func sendToMail(sender: AnyObject) {
      let composer = MFMailComposeViewController()
      composer.setSubject("Данные из JATO")
      composer.setMessageBody(cars.array.description, isHTML: false)
   }
 
   override func viewDidLoad() {
      super.viewDidLoad()
      cars.lift().bindTo(tableView) { indexPath, dataSource, tableView in
         let cell = tableView.dequeueReusableCellWithIdentifier("CarCell") as! CarTableViewCell
         cell.carLabel.text = dataSource[0][indexPath.row].description
         return cell
      }
   }
}
