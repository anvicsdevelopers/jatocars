//
//  Extensions.swift
//  MerryVideoEditor
//
//  Created by Nikita Arkhipov on 02.11.14.
//  Copyright (c) 2014 Anvics. All rights reserved.
//

import UIKit

func random(from from: UInt32, to: UInt32) -> Int{
    return Int(arc4random_uniform(to - from) + from)
}

func frandom(from from: CGFloat, to: CGFloat) -> CGFloat{
   return CGFloat(arc4random_uniform(UInt32(max(to - from + 1, 0)))) + from
}

func randomPointInRect(rect: CGRect) -> CGPoint{
   return CGPoint(x: frandom(from: rect.origin.x, to: rect.origin.x + rect.size.width), y: frandom(from: rect.origin.y, to: rect.origin.y + rect.size.height))
}

func randomBool() -> Bool{
    return (arc4random_uniform(2) == 1)
}

func randomTime(from from: CFTimeInterval, duration: CFTimeInterval) -> CFTimeInterval{
    return from + CFTimeInterval(arc4random_uniform(UInt32(duration * 1000))/1000)
}

func GCD_Main(block: () -> ()){
    dispatch_async(dispatch_get_main_queue(),{
        block()
    })
}

func GCD_Background(block: () -> ()){
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
        block()
    })
}

func GCD_After(seconds: Double, block: () -> ()){
    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(seconds * Double(NSEC_PER_SEC)))
    dispatch_after(delayTime, dispatch_get_main_queue()) {
        block()
    }
}

extension Bool{
    var oppositeValue: Bool { return !self }
    var intValue: Int { return self ? 1 : 0 }
}

extension String{
    func containsString(string: String) -> Bool{
        return rangeOfString(string) != nil
    }
   
   func indexOfCharacter(char: Character) -> Int? {
      if let idx = self.characters.indexOf(char) {
         return self.startIndex.distanceTo(idx)
      }
      return nil
   }
   
   func substringFromIndex(index: Int) -> String{
      return substringFromIndex(index, length: length - index)
   }
   
   func substringToIndex(index: Int) -> String{
      return substringFromIndex(0, length: index)
   }
   
   func substringFromIndex(index: Int, length: Int) -> String{
      return (self as NSString).substringWithRange(NSRange(location: index, length: length))
   }
   
   func stringByRemoving(removeStrings: [String]) -> String{
      return removeStrings.reduce(self) { $0.stringByReplacingOccurrencesOfString($1, withString: "") }
   }
   
   func stringByRemovingLast(numberOfChars: Int) -> String{
      return substringToIndex(length - numberOfChars)
   }
   
   var stringByRemovingSpaces: String { return stringByReplacingOccurrencesOfString(" ", withString: "") }
   
   var length: Int{ return self.characters.count }
}

extension NSDate{
    func dateStringWithFormat(format: String) -> String{
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        return formatter.stringFromDate(self)
    }
}

extension Int{
    var floatValue: Float{ return Float(self) }
   
   func bound(min minimun: Int, max maximum: Int) -> Int{
      if self < minimun { return minimun }
      if self > maximum { return maximum }
      return self
   }
}

extension Array{
   func safeObjectAt(index: Int) -> Element?{
      if index < 0 || index >= count { return nil }
      return self[index]
   }

   func safeObjectAt(index: Int?) -> Element?{
      guard let index = index else { return nil }
      if index < 0 || index >= count { return nil }
      return self[index]
   }

    mutating func switchElementsWithIndexes(index1 index1: Int, index2: Int){
        let tmp = self[index1]
        self[index1] = self[index2]
        self[index2] = tmp
    }
   
   func numberOf(ev: Element -> Bool) -> Int{
      return reduce(0) { s, el in ev(el) ? s + 1 : s }
   }
   
    func enumerateIndexed(callback:(Int, Element) -> ()){
        for (index, element) in self.enumerate(){
            callback(index, element)
        }
    }
    
    func each(callback:(Element) -> ()){
        for element in self{
            callback(element)
        }
    }
   
   func enumerateTwo(callback: (Element?, Element) -> ()){
      for i in 0..<count{
         callback(i == 0 ? nil : self[i-1], self[i])
      }
   }
   
   func findIndexForTwo(isTargetIndex: (Element, Element) -> Bool) -> Int?{
      for i in 1..<count{
         if isTargetIndex(self[i-1], self[i]) { return i-1 }
      }
      return nil
   }
   
   func findIndex(ev: Element -> Bool) -> Int?{
      for (index, el) in enumerate(){
         if ev(el) { return index }
      }
      return nil
   }
   
   mutating func exchange(index1 index1: Int, index2: Int){
      swap(&self[index1], &self[index2])
   }

   func arrayByInserting(element: Element, atIndex index: Int) -> [Element]{
      if index == 0 { return [element] + self }
      if index == count { return self + [element] }
      return self[0..<index] + [element] + self[index..<count]
   }
   
   func has(evaluator: Element -> Bool) -> Bool{
      for el in self{
         if evaluator(el) { return true }
      }
      return false
   }
   
   func divideInTwoArrays(shoudPutInFirstArray: Element -> Bool) -> ([Element], [Element]){
      var a1: [Element] = []
      var a2: [Element] = []
      for el in self{
         if shoudPutInFirstArray(el) { a1.append(el) }
         else { a2.append(el) }
      }
      return (a1, a2)
   }
   
   func find(ev: Element -> Bool) -> Element?{
      for el in self{
         if ev(el) { return el }
      }
      return nil
   }
   
   func split3() -> ([Element], Element, [Element]){
      precondition(count > 0)
      if count == 1 { return ([], self[0], []) }
      if count == 2 { return ([self[0]], self[1], []) }
      let center = count / 2
      return (Array(self[0..<center]), self[center], Array(self[center + 1..<count]))
   }

   func mapIndexed<U>(evaluator: (Element, Int) -> U) -> [U]{
      var newArray: [U] = []
      for (index, el) in enumerate(){
         newArray.append(evaluator(el, index))
      }
      return newArray
   }
}

extension Array where Element: Equatable{
   func arrayByAppendingUniq(value: Element) -> [Element]{
      return contains(value) ? self : arrayByInserting(value, atIndex: 0)
   }
}

func boundValue <T : Comparable> (value: T, minValue: T, maxValue: T) -> T{
    let a = max(value, minValue)
    return min(a, maxValue)
}

extension NSObject{
   func pointerString() -> String {
      let ptr: COpaquePointer = Unmanaged<AnyObject>.passUnretained(self).toOpaque()
      return "\(ptr)"
   }
}

func exchange<T>(inout data: [T], i: Int, j: Int) {
   swap(&data[i], &data[j])
}

func findFirst<T>(array1: [T], array2: [T], compare: (T, T) -> Bool) -> (T, T)?{
   for a1 in array1{
      for a2 in array2{
         if compare(a1, a2) { return (a1, a2) }
      }
   }
   return nil
}

//extension Any{
//   var isNilString: String { return "not nil" }
//}

func isNil<T>(v: T?) -> String{
   return v == nil ? "is nil" : "not nil"
}


extension NSTimeInterval{
   var formattedText: String{
      var tmp = Int(self)
      if tmp < 60 { return "0:\(formattedSeconds(tmp))" }
      var result = ""
      while tmp > 60{
         result += "\(tmp / 60):"
         tmp /= 60
      }
      return result + "\(formattedSeconds(tmp))"
   }
   
   private func formattedSeconds(s: Int) -> String{ return s < 10 ? "0\(s)" : "\(s)" }
   
   var superFormattedText: String{
      var tmp = Int(self)
      var result = ""
      if tmp > 3600{
         result += "\(tmp / 3600) h "
         tmp /= 3600
      }
      if tmp > 60 || result.length > 0{
         result += "\(tmp / 60) m "
         if tmp > 60 { tmp /= 60 }
      }
      return result + "\(tmp) s"
   }
}

func measure(name: String, block: () -> ()){
   let date = NSDate()
   block()
   print("\(name): \(NSDate().timeIntervalSinceDate(date))")
}

infix operator +? {}
func +?(left: String?, right: String) -> String?{
   if let left = left { return left + right }
   return nil
}

func +?(left: String, right: String?) -> String?{
   if let right = right { return left + right }
   return nil
}

func *(left: Int, right: Double) -> Double{
   return Double(left) * right
}

func *(left: Double, right: Int) -> Double{
   return Double(right) * left
}

enum Either<U, T>{
   case First(U)
   case Second(T)
}


func +<T>(left: T -> Bool, right: T -> Bool) -> T -> Bool{
   return { v in left(v) && right(v) }
}

func += <K, V> (inout left: [K:V], right: [K:V]) {
   for (k, v) in right {
      left.updateValue(v, forKey: k)
   }
}

func +<K, V> (left: [K:V], right: [K:V]) -> [K:V]{
   var dict = left
   for (k, v) in right {
      dict.updateValue(v, forKey: k)
   }
   return dict
}

extension String: CustomStringConvertible{
   public var description: String { return self }
}
