//
//  ToyotaAPI.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 08.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class JATOAPI: MagicsAPI{
   static let sharedAPI = JATOAPI()
   
   //override func baseURL() -> String { return "https://webapi-live.jato.com/jato.carspecs.api/" }
    override func baseURL() -> String { return "https://webapi-demo.jato.com/jato.carspecs.api/" }//
   
   override func configureRequest(request: NSMutableURLRequest) {
      if let token = JATOUser.mainUser.token.value{
         request.setValue("Basic \(token)", forHTTPHeaderField: "Authorization")
      }
   }
   
   func authorize() -> MagicsPipe{
//      if JATOUser.mainUser.token.value != nil { return MagicsPipe(API: self) }
      let currency = JATOCurrency()
      let language = JATOLanguage()
      return interact(JATOUser.mainUser).thenInteract(currency).thenInteract(language)
   }
   
//   override func logLevel() -> MagicsAPI.LogLevel { return .FullWithoutJSON }
}