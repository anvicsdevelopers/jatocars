//
//  JATO.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 06.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

final class JATOCar: MagicsInMemory{
   
   //MARK: Loaded from JATO
   let formatedPrice = Observable("")
   let make = Observable("")
   let name = Observable("")
   let price = Observable(0)
   let vehicleID = Observable("")//actually uid
   let id = Observable("")
   
   let engine = Observable("")
   let numberOfSeats = Observable("")
   let transmission = Observable("")
   let model = Observable("")
   let complectation = Observable("")
   
   let bodyCode = Observable("")
   let shortBodyName = Observable("")
   
   let imageURL = Observable("")
   
   let options = ObservableArray<JATOOption>()
   
   //MARK: Generated
   var numberID: NSNumber{ return NSNumber(longLong: Int64(id.value)!) }
   let buildedPrice = Observable(0)
   
   init(){
      price.map(PriceFormatter).bindTo(formatedPrice)
      
      options.observe { s in
         for o in s.sequence.filter({ $0.isBuildable }){
            o.isBuilded.distinct().observeNew { [weak self] isBuilded in
               self?.buildedPrice.value += o.price.value * (isBuilded ? 1 : -1)
            }
         }
      }
   }
   
   func update(key: String?, data: JSONData, parser: MagicsParser){
      make.update(data["makeNameGlobal"].string)
      name.update(data["makeNameToDisplay"].string + " " + data["modelNameToDisplay"].string)
      price.update(data["price"].int)
      id.update(data["vehicleId"].string)
      vehicleID.update(data["vehicleUid"].string)
    
      bodyCode.update(data["bodyCode"].string)
      shortBodyName.update(data["shortBodyName"].string)
      
      model.update(data["modelNameToDisplay"].string)
      complectation.update(data["derivativeToDisplay"].string)
      engine.update(data["engine"].string + " л, " + data["fuelTypeToDisplay"].string)//
      numberOfSeats.update(data["seats"].string)
      transmission.update(data["transmission"].string)
      
      buildedPrice.value = price.value
   }
   
    func buildOptionToCatchUp(car: JATOCar, availableOptions: [JATOOption], shouldBuild: Bool = true){
      for option in availableOptions.filter({ $0.isBuildable }){
         let carOption = car.options.array.find { $0.key.value == option.key.value }
         if carOption?.isIncluded.value ?? false { option.isBuilded.value = shouldBuild }
      }
   }
   
   static func isSortable() -> Bool { return true }
   static func isOrderedBefore(left: JATOCar, right: JATOCar) -> Bool { return left.price.value < right.price.value }
}

extension JATOCar: CustomStringConvertible{
   var description: String{ return "\(name.value): \(complectation.value) [\(bodyCode.value) - \(shortBodyName.value)] (\(PriceFormatter(price.value))) --> \(vehicleID.value)\n" }
//   var description: String{ return "\(name.value): \(complectation.value) (\(PriceFormatter(price.value))) --> \(imageURL.value)\n" }
}

extension JATOCar{
   enum MakeName: String{
      case Toyota = "TOYOTA"
      case LandRover = "LAND ROVER"
      case Audi = "AUDI"
      case Mercedes = "MERCEDES"
      case AstonMartin = "ASTON MARTIN"
      case Mitsubishi = "MITSUBISHI"
      case KIA = "KIA"
      case Hundai = "HYUNDAI"
      case Mazda = "MAZDA"
      case Nissan = "NISSAN"
      case Volkswagen = "VOLKSWAGEN"
   }
}

func completePrice(price: Int) -> String{
   if price > 100 { return "\(price)" }
   if price > 10 { return "0\(price)" }
   return "00\(price)"
}

func PriceFormatter(price: Int) -> String{
   let mil = price / 1000000
   let th = (price - mil * 1000000) / 1000
   let v = price - mil * 1000000 - th * 1000
   if mil == 0 { return "\(th) \(completePrice(v)) ₽" }
   return "\(mil) \(completePrice(th)) \(completePrice(v)) ₽"
}


