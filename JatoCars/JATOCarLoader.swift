//
//  JATOCarLoader.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 09.08.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class JATOCarLoader: MagicsInteractor{
   typealias CarArray = ObservableArray<JATOCar>

   let cars = CarArray()
   
   let toyota = CarArray()
   let competitors = CarArray()

   var comparator: JatoComparator?
   
   private let bodies: ObservableArray<JatoCarBody>
   
   init(bodies: ObservableArray<JatoCarBody>){
      self.bodies = bodies
      cars.observe { [unowned self] cars in
         self.toyota.array = cars.sequence.filter { $0.make.value == JATOCar.MakeName.Toyota.rawValue }
         self.competitors.array = cars.sequence.filter { $0.make.value != JATOCar.MakeName.Toyota.rawValue }
      }
   }
   
   
   func fireWhenReady(fireSignal: Signal) {
      bodies.observe {
         if $0.sequence.count > 0 { fireSignal.next() }
      }
   }
   
   func getURL() -> String{ return "vehicles/v2/load" }
   
   func configureRequest(request: NSMutableURLRequest){
      let info = ["databaseName": "SSCRUS_CS2002", "selections": bodies.array.map(bodyMap)]
      request.setJSONBodyWithParams([info])
   }
   
   func bodyMap(body: JatoCarBody) -> [String: String]{
      return ["makeName": body.makeName.value,
              "modelName": body.modelName.value,
              "modelYear": body.modelYear.value,
              "numberOfDoors": body.numberOfDoors.value,
              "bodyCode": body.bodyCode.value,
              "drivenWheels": body.drivenWheels.value]
   }
   
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool{
      return data != nil && error == nil
   }
   
   func processResult(data: JSONData?){
      guard let data = data else { fatalError() }
      
      func sorter(car1: JATOCar, car2: JATOCar) -> Bool{
         if car1.make.value == car2.make.value { return car1.price.value < car2.price.value }
         return car1.make.value > car2.make.value
      }
      
      let parser = MagicsParser()
      let tmpArray = CarArray()
      parser.updateWithJSON(data[0]["vehicles"], array: tmpArray)
      if let comparator = comparator { tmpArray.array = tmpArray.filter(comparator.shouldLoadFromJATO) }
      cars.array = tmpArray.array.sort(sorter)
      print(cars.array)
   }
}