//
//  JatoLanguage.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 08.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

final class JATOCurrency: MagicsInteractor{
   var currency = "RUR"
   
   required init(){}

   func getURL() -> String{ return "currencies/set" }
   
   func configureRequest(request: NSMutableURLRequest){
      request.setJSONBodyWithText("\"\(currency)\"")
   }
}