//
//  JATOLanguage.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 11.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

final class JATOLanguage: MagicsInteractor{
   var languageID = 36
   
   required init(){}
   
   func getURL() -> String{ return "languages/set" }
   
   func configureRequest(request: NSMutableURLRequest){
      request.setJSONBodyWithText("\(languageID)")
   }
}