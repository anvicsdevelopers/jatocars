//
//  JATOLoader.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 28.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import UIKit

class JATOLoader{
   typealias CarArray = ObservableArray<JATOCar>

//   static let sharedLoader = JATOLoader()
   
   let statusText = Observable("")
   
   let isLoading = Observable(false)
   let isLoadedCars = Observable(false)
   let isLoaded = Observable(false)
   let hasFailedLoading = Observable(false)
   
   let reloadSignal = EventProducer<JATOCarLoader>()
   
   init(){
      reloadSignal.observe { [unowned self] loader in
         self.load(loader)
      }
   }
   
   private func setUpForLoadingWithStatus(status: String){
      statusText.value = status
      isLoading.value = true
      isLoaded.value = false
      hasFailedLoading.value = false
   }
   
   private func decorateAndRunPipe(pipe: MagicsPipe, states: [String]){
      pipe.follow().map { states[$0] }.bindTo(statusText)
      
      pipe.onComplete().rewrite(true).bindTo(isLoaded)
      pipe.onComplete().rewrite(false).bindTo(isLoading)
      
      pipe.onError().rewrite("При загрузке произошла ошибка. Повторите попытку позже").bindTo(statusText)
      pipe.onError().rewrite(true).bindTo(hasFailedLoading)
      pipe.onError().rewrite(false).bindTo(isLoading)
      
      pipe.run()
   }

   func load(loader: JATOCarLoader){
      if isLoading.value || isLoadedCars.value { return }
      isLoadedCars.value = false
      setUpForLoadingWithStatus("Авторизация...")
      
      let pipe = JATOAPI.sharedAPI.authorize().thenInteract(loader)
      pipe.onComplete().rewrite(true).bindTo(isLoadedCars)

      decorateAndRunPipe(pipe, states: ["Выставление валюты...", "Выставление языка...", "Загрузка автомобилей...", "Готово!"])
   }
   
   func loadOptionsForCars(cars: [JATOCar], completion: EmptyBlock){
      if isLoading.value { return }
      print("Loading options for \(cars)")
      setUpForLoadingWithStatus("Загрузка опций...")
      
      let pipe = JATOAPI.sharedAPI.interact(JATOOptionsLoader(cars: cars))
      pipe.onComplete().observe(completion)
      decorateAndRunPipe(pipe, states: ["Готово!"])
   }
}