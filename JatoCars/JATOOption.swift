//
//  JATOOption.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 28.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

final class JATOOption: MagicsInMemory{
   let key = Observable("")
   let value = Observable("")
   
   let optionID = Observable(0)
   let optionCode = Observable("")
   
   let isIncluded = Observable(false)
   
   let category = Observable("")
   
   let price = Observable(0)
   let requires = ObservableArray<Int>()
   let includes = ObservableArray<Int>()
   let excludes = ObservableArray<Int>()
   
   var isBuildable: Bool { return optionID.value != 0 }
   private var representedAsBuildable: Bool { return optionID.value != 0 }
   var formattedValue: String{ return isBuildable ? PriceFormatter(price.value) : value.value }
   let isBuilded = Observable(false)
   
   let showAsBuilded = Observable(false)
   
   required init(){
      combineLatest(isIncluded, isBuilded)
         .map { i, b in i || b }
         .bindTo(showAsBuilded)
   }
   
   func update(key: String?, data: JSONData, parser: MagicsParser){
      self.key.update(JATOOption.updateEscaped(data["ebrochureText"].string))
      self.value.update(JATOOption.validateValue(data["formattedValues"][0]["label"]))
      self.category.update(data["translatedCategoryName"].string)
      self.isIncluded.update(value.value != "Недоступна")
      self.optionID.update(data["formattedValues"][0]["optionId"].int)
      self.optionCode.update(data["formattedValues"][0]["optionCode"].string)
      
//      if isBuildable { print("++\(optionID.value): \(optionCode.value)") }
    
      if self.category.value == "category_014"{
         self.category.value = "Эксплуатационные характеристики"
      }
   }
   
   func updateForBuild(data: JSONData){
      guard data["optionId"].int == optionID.value else { fatalError("updateForBuild incorrect id") }
      price.update(data["price"].int)
      addData(data["includes"], toArray: requires)
      addData(data["requires"], toArray: includes)
      addData(data["excludes"], toArray: excludes)
   }
   
   private func addData(data: JSONData, toArray: ObservableArray<Int>){
      data.enumerate { _, v in
         if v.isDictionary { //Хуже АПИ я ни разу в жизни не видел!
            self.addData(v["optionId"], toArray: toArray)
         }else{
            toArray.append(v.int)
         }
      }
   }
   
   private static func updateEscaped(st: String?) -> String?{
      if let st = st{
         return st.stringByRemoving(["\"", "\n", "<i>", "</i>"]).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
      }
      return nil
   }
   
   private static func validateValue(value: JSONData) -> String?{
      if let bool = value.bool { return bool ? "да" : "нет" }
      if let int = value.int { return "\(int)" }
      if let st = value.string where st != "" { return updateEscaped(st) }
      return nil
   }
}

extension JATOOption: CustomStringConvertible{
   var description: String { return "\(key.value)(\(optionID.value)): \(value.value)" }
}
