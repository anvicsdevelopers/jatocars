//
//  JATOComparator.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 11.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class JATOOptionsLoader: MagicsInteractor{
   let cars: ObservableArray<JATOCar>

   init(cars: [JATOCar]){
      self.cars = ObservableArray(cars)
   }
   
   init(cars: ObservableArray<JATOCar>){
      self.cars = cars
   }
   
   func getURL() -> String{ return "vehiclesbuilder/build" }
   
   func fireWhenReady(fireSignal: Signal) {
      cars.observe {
         if $0.sequence.count > 0 { fireSignal.next() }
      }
   }
   
   func configureRequest(request: NSMutableURLRequest) {
      request.setJSONBodyWithParams(JATOOptionsLoader.dataForCars(cars.array))
   }
   
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool {
      return data != nil && error == nil
   }
   
   func processResult(data: JSONData?){
      guard let data = data else { return }
      let parser = MagicsParser()
      data.enumerate { _, data in
         let car = self.carForData(data)
         let options = car.options
         parser.updateWithJSON(data["vehicleEbrochurePage"]["pageItem"], array: options)
         self.updateOptions(options, withBuildData: data["vehicleOptionPage"]?["optionInfos"])
         car.imageURL.update(data["vehicleHeaderInfo"]["vehiclePhotoPath"]["photoPath"].string)
      }
   }
   
   private func carForData(data: JSONData) -> JATOCar{
    let id = data["vehicleId"].string
    let uid = id.substringToIndex(id.characters.count - 8)
      return cars.array.find { $0.vehicleID.value == uid }!
   }
   
   private func updateOptions(options: ObservableArray<JATOOption>, withBuildData buildData: JSONData?){
      guard let buildData = buildData else { return }
      buildData.enumerate { _, data in
//         print("--\(data["optionId"].int): \(data["optionCode"].string)")
         self.optionsForData(data, inArray: options).each {
            $0.updateForBuild(data)
//            if $0.optionCode.value != data["optionCode"].string { print("###\(data["optionId"].int): \(data["optionCode"].string) -> \($0.optionID.value): \($0.optionCode.value)") }
         }
      }
      
      //Test ensurance
//      print("Checking that all options has price..")
//      for o in options.array{
//         if o.isBuildable && o.price.value == 0 { print("-No price for \(o.optionID.value)") }
//      }
//      print("YES")
   }
   
   private func optionForData(data: JSONData, inArray: ObservableArray<JATOOption>) -> JATOOption?{
      let id = data["optionId"].int
      return inArray.array.find { $0.optionID.value == id }
   }

   private func optionsForData(data: JSONData, inArray: ObservableArray<JATOOption>) -> [JATOOption]{
      let id = data["optionId"].int
      return inArray.array.filter { $0.optionID.value == id }
   }

   static func dataForCars(cars: [JATOCar]) -> AnyObject{
      guard cars.count > 0 else { fatalError() }
      
      let vehicles = cars.map { ["vehicleId": $0.numberID] }//[vehicle1, vehicle2]
      let market = ["databaseName": "SSCRUS_CS2002", "vehicles": vehicles]
      let data = ["benchmarkVehicleId": cars[0].numberID, "markets": [market]]
      return data
   }
}


