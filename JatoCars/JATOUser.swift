//
//  JATOAuth.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 08.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

final class JATOUser: MagicsInteractor{
   static var mainUser = JATOUser()
   
   let email = Observable("rrostovcev@toyota-motor.ru")
   let password = Observable("high7octane")
   
   let isAuthorized = Observable(false)
   
   let token = Observable<String?>(nil)
   
   init(){
      token.map { $0 != nil }.bindTo(isAuthorized)
   }
   
   func getURL() -> String{ return "authentication/login" }
   
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool{
      return data != nil
   }
   
   func processResult(data: JSONData?){
      guard let data = data else { fatalError() }
      token.value = data["token"].string
      print("token = \(data["token"].string)")
   }
   
   func configureRequest(request: NSMutableURLRequest){
      request.setJSONBodyWithParams(["email": email.value, "password": password.value])
   }
}