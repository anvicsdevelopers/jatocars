//
//  JatoBodiesLoader.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 29.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class JatoBodiesLoader: MagicsInteractor{
   
   let bodies = ObservableArray<JatoCarBody>()

   private let makes: [JatoCarModel]
   init(makes: [JatoCarModel]){
      self.makes = makes
   }

   func getURL() -> String{ return "bodies/v2/load" }
   
   func configureRequest(request: NSMutableURLRequest){
      let info = ["databaseName": "SSCRUS_CS2002",
                  "selections": makes
                     .map { make in ["makeName": make.make.value, "modelName": make.model.value, "modelYear": "\(make.year.value)"] }]
      
      request.setJSONBodyWithParams([info])
   }
   
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool{
      return data != nil && error == nil
   }
   
   func processResult(data: JSONData?){
      guard let data = data else { fatalError() }
      
      print(data)
      
      let parser = MagicsParser()

      parser.updateWithJSON(data[0]["bodies"], array: bodies)
      
      print("Requesting following:")
      for b in bodies.array{
         let make = b.makeName.value
         let model = b.modelName.value
         let year = b.modelYear.value
         let doors = b.numberOfDoors.value
         let code = b.bodyCode.value
         let wheels = b.drivenWheels.value
         
         let name = (make+model+year+doors+code+wheels).stringByRemoving([" "])
         print("let \(name) = [\"makeName\": \"\(make)\", \"modelName\": \"\(model)\", \"modelYear\": \"\(year)\", \"numberOfDoors\": \"\(doors)\", \"bodyCode\": \"\(code)\", \"drivenWheels\": \"\(wheels)\"]")
      }
   }
   
}

final class JatoCarBody: MagicsInMemory, CustomStringConvertible{
   let makeName = Observable("")
   let modelName = Observable("")
   let modelYear = Observable("")
   let numberOfDoors = Observable("")
   let bodyCode = Observable("")
   let drivenWheels = Observable("")
   
   required init(){}
   
   func update(key: String?, data: JSONData, parser: MagicsParser) {
      makeName.update(data["makeKey"].string)
      modelName.update(data["modelKey"].string)
      modelYear.update(data["modelYear"].string)
      numberOfDoors.update(data["numberOfDoors"].string)
      bodyCode.update(data["bodyCode"].string)
      drivenWheels.update(data["drivenWheels"].string)
   }
   
   var description: String { return makeName.value + " " + modelName.value + " " + modelYear.value + " \(numberOfDoors.value)|\(bodyCode.value)|\(drivenWheels.value)"}
}
