//
//  JatoCarCompareHelper.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 01.08.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation
 
protocol JatoComparator{
   var items: [JatoComparableItems] { get }
   
   func shouldLoadFromJATO(car: JATOCar) -> Bool
   func canCompareModel(model: JATOCar, withCompetitor competitor: JATOCar) -> Bool
}

extension JatoComparator{
   func shouldLoadFromJATO(car: JATOCar) -> Bool{
      for item in items{
         if item.containsCar(car) { return true }
      }
      return false
   }
   func canCompareModel(model: JATOCar, withCompetitor competitor: JATOCar) -> Bool{
      for item in items{
         if item.containsToyota(model) && item.containsCompetitor(competitor) { return true }
      }
      return false
   }
}

struct JatoComparableItems{
   let toyotaIDs: [String]
   let competitorIDs: [String]
    
    init(toyotaIDs: [String], competitorIDs: [String], shouldMap: Bool = true){
        func idMap(s: String) -> String { return shouldMap && s.length > 12 ? s.substringToIndex(s.characters.count - 8) : s }
        self.toyotaIDs = toyotaIDs.map(idMap)
        self.competitorIDs = competitorIDs.map(idMap)
    }
   
   func containsCar(car: JATOCar) -> Bool{
      let id = car.vehicleID.value
      return toyotaIDs.contains(id) || competitorIDs.contains(id)
   }
   
   func containsToyota(toyota: JATOCar) -> Bool{
      return toyotaIDs.contains(toyota.vehicleID.value)
   }
   
   func containsCompetitor(competitor: JATOCar) -> Bool{
      return competitorIDs.contains(competitor.vehicleID.value)
   }
}

func JCI(toyotaIDs: [String], _ competitorIDs: [String]) -> JatoComparableItems{
   return JatoComparableItems(toyotaIDs: toyotaIDs, competitorIDs: competitorIDs)
}


func JCI(toyotaIDs: [Int], _ competitorIDs: [Int]) -> JatoComparableItems{
   return JatoComparableItems(toyotaIDs: toyotaIDs.map(String.init), competitorIDs: competitorIDs.map(String.init))
}

func JCI(toyotaIDs: [String], _ competitorIDs: [Int]) -> JatoComparableItems{
   return JatoComparableItems(toyotaIDs: toyotaIDs, competitorIDs: competitorIDs.map(String.init))
}



