//
//  JatoMakesLoader.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 28.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class JatoMakesLoader: MagicsInteractor{

   let makes = ObservableArray<JatoMake>()
   
   init(){}
   
   func getURL() -> String{ return "makes/v2/load" }
   
   func configureRequest(request: NSMutableURLRequest){
      let info = ["databaseName": "SSCRUS_CS2002"]
      request.setJSONBodyWithParams([info])
   }
   
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool{
      return data != nil && error == nil
   }
   
   func processResult(data: JSONData?){
      guard let data = data else { fatalError() }
      
      let parser = MagicsParser()
      parser.updateWithJSON(data[0]["makes"], array: makes)

      print(makes.array)
   }
   
}

final class JatoMake: MagicsInMemory, CustomStringConvertible{
   let makeKey = Observable("")
   
   required init(){}
   
   func update(key: String?, data: JSONData, parser: MagicsParser){
      makeKey.update(data["makeKey"].string)
   }
   
   var description: String { return makeKey.value }
}
