//
//  JatoModelsLoader.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 28.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class JatoModelsLoader: MagicsInteractor{
   
   let makes: ObservableArray<JatoMake>
   let models = ObservableArray<JatoCarModel>()
   
   init(makes: ObservableArray<JatoMake>){
      self.makes = makes
   }
   
   func getURL() -> String{ return "models/v2/load" }
   
   func configureRequest(request: NSMutableURLRequest){
      let info = ["databaseName": "SSCRUS_CS2002", "selections": makes.array.map { ["name": $0.makeKey.value] }]
      request.setJSONBodyWithParams([info])
   }
   
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool{
      return data != nil && error == nil
   }
   
   func processResult(data: JSONData?){
      guard let data = data else { fatalError() }
      
      func sorter(car1: JatoCarModel, car2: JatoCarModel) -> Bool{
         if car1.make.value == car2.make.value { return car1.model.value < car2.model.value }
         return car1.make.value < car2.make.value
      }

      let parser = MagicsParser()
      
      let tmpArray = ObservableArray<JatoCarModel>()
      parser.updateWithJSON(data[0]["models"], array: tmpArray)
      tmpArray.array.sortInPlace(sorter)
      
      models.array = tmpArray.array
      
      print(models.array)
   }
}

final class JatoCarModel: MagicsInMemory, CustomStringConvertible{
   let make = Observable("")
   let model = Observable("")
   let year = Observable(0)
   
   required init(){ }
   
   func update(key: String?, data: JSONData, parser: MagicsParser) {
      make.update(data["makeKey"].string)
      model.update(data["modelKey"].string)
      year.update(data["modelYear"].int)
   }
   
   var title: String { return model.value + " \(year.value)" }
   
   var description: String { return make.value + " " + model.value + " \(year.value)" }
}


