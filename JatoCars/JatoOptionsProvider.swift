//
//  JatoCompeHelper.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 31.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

protocol JatoOptionsProvider {
   func sectionItemsFor(car1 car1: JATOCar, car2: JATOCar, isEquipment: Bool) -> [JatoOptionsSection]
}

struct JatoOptionsSection{
   var title: String{ return toyotaOptions[0].category.value }
   let toyotaOptions: [JATOOption]
   let competitorOptions: [JATOOption]
}




