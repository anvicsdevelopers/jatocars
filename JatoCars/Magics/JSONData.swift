//
//  JSONData.swift
//  APIMagic
//
//  Created by Nikita Arkhipov on 30.10.15.
//  Copyright © 2015 Anvics. All rights reserved.
//

import Foundation

class JSONData {
   let data: AnyObject
   
   var isArray: Bool { return data is NSArray }
   var isDictionary: Bool { return data is NSDictionary }
   var isEnumaratable: Bool{ return isArray || isDictionary }
   
   var array: NSArray! { return data as? NSArray }
   var dictionary: NSDictionary! { return data as? NSDictionary }
   var int: Int! { return (data as? Int) ?? (string != nil ? Int(string) : nil) }
   var string: String! { return (data as? String) ?? (data as? NSNumber != nil ? String(data as! NSNumber) : nil) }
   var bool: Bool! { return data as? Bool }
   var double: Double! { return (data as? Double) ??  (string != nil ? Double(string) : nil) }
   
   init(data: AnyObject){
      self.data = data
   }
   
   subscript(index: Int) -> JSONData{
      guard let array = data as? NSArray else{
         fatalError()
      }
      
      return JSONData(data: array[index])
   }
   
   subscript(key: String) -> JSONData!{
      guard let dict = data as? NSDictionary else{
         return nil
      }
      if let v = dict[key]{ return JSONData(data: v) }
      else{ return nil }
   }
   
   func enumerate(callback: (String?, JSONData)->()){
      if let array = array{
         for data in array{
            callback(nil, JSONData(data: data))
         }
      }else if let dict = dictionary{
         for key in dict.allKeys as! [String]{
            callback(key, JSONData(data: dict[key]!))
         }
      }
   }
   
   static func getFrom(url: String, completion: (JSONData?, NSError?) -> Void){
      getFrom(url, modifyRequest: { _ in }, completion: completion)
   }

   static func getFrom(url: String, logLevel: MagicsAPI.LogLevel = .None, modifyRequest: NSMutableURLRequest -> Void, completion: (JSONData?, NSError?) -> Void){
      func mprint(string: String){
         guard logLevel != .None else { return }
         print(string)
      }
      func jprint(string: String){
         guard logLevel == .Full || logLevel == .JSONOnly else { return }
         print(string)
      }
      mprint("JSONData.getFrom: \(url)")
      guard let nsurl = NSURL(string: url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) ?? "") else{
         mprint("Failed to create NSURL")
         completion(nil, NSError(domain: "ru.Anvics.InvalidURL", code: 0, userInfo: ["invalidURL": url]))
         return
      }

      let request = NSMutableURLRequest(URL: nsurl)
      modifyRequest(request)
      mprint("Started request")
      let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
         if let error = error {
            mprint("failed, recieved error: \(error)")
            JSON_GCD_Main { completion(nil, error) }
            return
         }
         if let response = response as? NSHTTPURLResponse where response.statusCode != 200{
            mprint("failed, invalid status code (should be 200): \(response.statusCode)")
            JSON_GCD_Main { completion(nil, NSError(domain: "ru.Anvics.InvalidStatusCode", code: 2, userInfo: ["statusCode": response.statusCode])) }
            return
         }
         
         if let response = data,
          let json = try? NSJSONSerialization.JSONObjectWithData(response, options: .AllowFragments){
            mprint("success")
            jprint(json.description ?? "")
            JSON_GCD_Main { completion(JSONData(data: json), nil) }
            return
         }
         mprint("failed, failed to create JSON object from response")
         JSON_GCD_Main { completion(nil, nil) }
      }
      task.resume()
   }
}

extension JSONData: CustomStringConvertible{
   var description: String {
      return data.description
   }
}

func JSON_GCD_Main(block: () -> ()){
   dispatch_async(dispatch_get_main_queue(),{
      block()
   })
}


extension JSONData{
   func printRecursively(deepLevel: Int, prefix: String = ""){
      var i = 0
      enumerate { key, value in
         if let key = key{
            print("\(prefix)\(key):")
         }else{
            print("\(prefix)[\(i)]:")
            i+=1
         }
         if deepLevel > 0 { value.printRecursively(deepLevel - 1, prefix: prefix + "-") }
      }
   }
}

extension NSMutableURLRequest{
   func setJSONBodyWithParams(params: AnyObject){
      HTTPMethod = "POST"
      HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
      addValue("application/json", forHTTPHeaderField: "Content-Type")
//      print(NSString(data: HTTPBody!, encoding: NSUTF8StringEncoding)!)
   }
   
   func setJSONBodyWithText(text: String){
      HTTPMethod = "POST"
      HTTPBody = text.dataUsingEncoding(NSUTF8StringEncoding)!
      addValue("application/json", forHTTPHeaderField: "Content-Type")
   }
   
   func setBodyWithParams(params: [String: String]){
      var body = ""
      for (key, value) in params{
         body += "&\(key)=\(value)"
      }
      HTTPMethod = "POST"
      HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)!
   }
}

