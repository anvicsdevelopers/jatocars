//
//  MagicsAPI.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 08.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

func +<T>(left: T -> Void, right: T -> Void) -> T -> Void{
   return { value in left(value); right(value) }
}

class MagicsAPI{
   var parser: MagicsParser = MagicsParser()
   
   //MARK: - can be overriden
   func baseURL() -> String { return "" }
   
   func configureRequest(request: NSMutableURLRequest){ }
   
   func logLevel() -> LogLevel { return .None }
   
   //MARK: - Public update section
   func update<T: MagicsInMemory where T: MagicsUndependable>(object: T) -> EventProducer<LoadingStatus>{
      return statusForUpdating(object, url: T.getURL())
   }
   
   func update<T: MagicsInMemory where T: MagicsUndependable>(array: ObservableArray<T>) -> EventProducer<LoadingStatus>{
      return statusForUpdating(array, url: T.getURL())
   }
   
   func update<T: MagicsInMemory where T: MagicsDependable>(array: ObservableArray<T>, root: T.Root) -> EventProducer<LoadingStatus>{
      return statusForUpdating(array, url: T.getURL(root))
   }
   
   func interact<T: MagicsInteractor>(interactor: T) -> MagicsPipe{
      return MagicsPipe(task: MagicsInteractionTask(interactor: interactor, API: self), API: self)
   }
}

//MARK: - Updates
extension MagicsAPI{
   private func processJSONLoading(url: String, modifyRequest: NSMutableURLRequest -> Void, statusProducer: EventProducer<LoadingStatus>, resultBlock: JSONData -> Void){
      statusProducer.next(.RequestStarted)
      JSONData.getFrom(baseURL() + url, logLevel: logLevel(), modifyRequest: configureRequest + modifyRequest) { data, error in
         if let data = data{
            statusProducer.next(.RequestSuccessed)
            resultBlock(data)
         }else{
            statusProducer.next(.RequestFailed(error))
         }
      }
   }
   
   private func updateFromURL<T: MagicsInMemory>(url: String, array: ObservableArray<T>, statusProducer: EventProducer<LoadingStatus>){
      processJSONLoading(url, modifyRequest: T.configureRequest, statusProducer: statusProducer) { json in
         self.parser.updateWithJSON(T.extractJSONFromJSON(json), array: array)
      }
   }
   
   func statusForUpdating<T: MagicsInMemory>(array: ObservableArray<T>, url: String) -> EventProducer<LoadingStatus>{
      let statusProducer = EventProducer<LoadingStatus>()
      array.array = []
      updateFromURL(url, array: array, statusProducer: statusProducer)
      return statusProducer
   }
    
   func statusForUpdating<T: MagicsInMemory>(object: T, url: String) -> EventProducer<LoadingStatus>{
      let statusProducer = EventProducer<LoadingStatus>()
      processJSONLoading(url, modifyRequest: T.configureRequest, statusProducer: statusProducer) { json in
         self.parser.updateWithJSON(T.extractJSONFromJSON(json), object: object)
      }
      return statusProducer
   }
}

extension MagicsAPI{
   enum LogLevel{
      case None, FullWithoutJSON, Full, JSONOnly
   }
   
   func mprint(string: String){
      guard logLevel() != .None else { return }
      print(string)
   }
   
   func mprint(string: CustomStringConvertible){
      guard logLevel() != .None else { return }
      print(string.description)
   }

   func mprint(string: CustomStringConvertible?){
      guard let st = string where logLevel() != .None else { return }
      print(st.description)
   }

   func mobserve(event: EventProducer<LoadingStatus>) {
      guard logLevel() != .None else { return }
      event.observe { print($0) }
   }
   
   func jprint(string: NSString?){
      guard let st = string where logLevel() == .Full || logLevel() == .JSONOnly else { return }
      print(st)
   }

   func jprint(string: String){
      guard logLevel() == .Full || logLevel() == .JSONOnly else { return }
      print(string)
   }

}

extension MagicsAPI{
   func setJSONBodyWithParams(params: AnyObject, request: NSMutableURLRequest){
      request.HTTPMethod = "POST"
      request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
      jprint(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding)!)
   }
   
   func setJSONBodyWithText(text: String, request: NSMutableURLRequest){
      request.HTTPMethod = "POST"
      request.HTTPBody = text.dataUsingEncoding(NSUTF8StringEncoding)!
      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
      jprint(text)
   }
   
   func setBodyWithParams(params: [String: String], request: NSMutableURLRequest){
      var body = ""
      for (key, value) in params{
         body += "&\(key)=\(value)"
      }
      request.HTTPMethod = "POST"
      request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)!
      mprint("POST body: \(body)")
   }
}