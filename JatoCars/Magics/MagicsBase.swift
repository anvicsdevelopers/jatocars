//
//  MagicsBase.swift
//  APIMagic
//
//  Created by Nikita Arkhipov on 30.10.15.
//  Copyright © 2015 Anvics. All rights reserved.
//

import Foundation

protocol LoadingsStatusProtocol {
   var requestError: NSError? { get }
   var hasSuccessfulyLoaded: Bool{ get }
   var hasFailedLoading: Bool{ get }
}

enum LoadingStatus: LoadingsStatusProtocol{
   case LoadedFromCash
   case RequestStarted, RequestSuccessed
   case RequestFailed(NSError?)
   
   var hasStartedRequest: Bool{
      switch self {
      case .RequestStarted: return true
      default: return false
      }
   }

   var hasFinishedLoading: Bool{
      switch self {
      case .LoadedFromCash, .RequestSuccessed, .RequestFailed(_): return true
      default: return false
      }
   }

   var hasSuccessfulyLoaded: Bool{
      switch self {
      case RequestSuccessed: return true
      default: return false
      }
   }

   var hasFailedLoading: Bool{
      switch self {
      case .RequestFailed(_): return true
      default: return false
      }
   }
   
   var requestError: NSError? {
      switch self {
      case .RequestFailed(let error): return error
      default: return nil
      }
   }
}

extension EventProducer where Event: LoadingsStatusProtocol{
   func onSuccess() -> Signal{
      return filter { $0.hasSuccessfulyLoaded }.signal()
   }
   
   func onError() -> EventProducer<NSError?>{
      return filter { $0.hasFailedLoading }.map { $0.requestError }
   }
}

//enum ItemStatus{
//   case Initial
//   case Added
//   case Updated
//   case Deleted
//}

enum CacheType{
//   case Default   //using ?since={NSTimeInterval since 1970 GMT +0}
   case Forced    //compare result of the same querry with cached data
   case DeleteAll //delete all cached data when recieves new
}

typealias ProgressBlock = (LoadingStatus) -> ()

protocol MagicsUpdatable {
   func update(key: String?, data: JSONData, parser: MagicsParser)
}



extension Observable where Wrapped: Equatable{
   func update(newValue: Wrapped){
      if value != newValue { value = newValue }
   }
   
   func update(newValue: Wrapped?){
      if let newValue = newValue where value != newValue{ value = newValue }
   }
}



