//
//  MagicsInteractor.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 09.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

protocol MagicsInteractor{
   func getURL() -> String
   
   func fireWhenReady(fireSignal: Signal)

   func configureRequest(request: NSMutableURLRequest)

   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool
   func processResult(data: JSONData?)
}

extension MagicsInteractor{
   func fireWhenReady(fireSignal: Signal) { fireSignal.next() }
   func configureRequest(request: NSMutableURLRequest){}
   func interactionIsSuccess(data: JSONData?, error: NSError?) -> Bool{ return error == nil }
   func processResult(data: JSONData?){ }
}


