//
//  MagicsProtocols.swift
//  CrossClub
//
//  Created by Nikita Arkhipov on 06.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

//MARK: Base model
protocol MagicsModel: MagicsUpdatable{
   func delete()
   static func createNew() -> Self

   static func shouldSkipData(data: JSONData, key: String?) -> Bool
   static func extractJSONFromJSON(json: JSONData) -> JSONData
   static func configureRequest(request: NSMutableURLRequest)
   
   static func isSortable() -> Bool
   static func isOrderedBefore(left: Self, right: Self) -> Bool
   
   static func isGlobal() -> Bool
}

extension MagicsModel{
   static func shouldSkipData(data: JSONData, key: String?) -> Bool{ return false }
   static func extractJSONFromJSON(json: JSONData) -> JSONData { return json }
   static func configureRequest(request: NSMutableURLRequest){}

   static func isSortable() -> Bool{ return false }
   static func isOrderedBefore(left: Self, right: Self) -> Bool{ fatalError() }
   
   static func isGlobal() -> Bool{ return false }
}

//MARK: - In memory model
protocol MagicsInMemory: MagicsModel {
   init()
}

extension MagicsInMemory{
   func delete(){}
   static func createNew() -> Self{ return Self() }
}

protocol MagicsUndependable{
   static func getURL() -> String
}

protocol MagicsDependable {
   associatedtype Root
   
   static func getURL(root: Root) -> String
}

