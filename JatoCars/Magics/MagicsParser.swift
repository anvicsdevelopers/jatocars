//
//  MagicsDecoder.swift
//  APIMagic
//
//  Created by Nikita Arkhipov on 30.10.15.
//  Copyright © 2015 Anvics. All rights reserved.
//

import Foundation

class MagicsParser{
   
   func updateWithJSON<T: MagicsInMemory>(json: JSONData, object: T){
//      print(json)
      if T.shouldSkipData(json, key: nil) { return }
      object.update(nil, data: json, parser: self)
   }

   func updateWithJSON<T: MagicsInMemory>(json: JSONData, object: Observable<T?>){
//      print(json)
      if T.shouldSkipData(json, key: nil) { return }
      let obj = object.value ?? T.createNew()
      obj.update(nil, data: json, parser: self)
      object.value = obj
   }
   
   func updateWithJSON<T: MagicsInMemory>(json: JSONData, array: ObservableArray<T>){
//      print(json)
      array.performBatchUpdates { array in
         json.enumerate() { key, data in
            if T.shouldSkipData(data, key: key) { return }
            let object = T.createNew()
            object.update(key, data: data, parser: self)
            self.insertObject(object, inArray: array)
         }
      }
   }
 }

//MARK: - Processing helpers
extension MagicsParser{
   private func insertObject<T: MagicsModel>(object: T, inArray array: ObservableArray<T>){
      if T.isSortable() { array.insert(object, atIndex: self.insertIndex(object, inArray: array.array)) }
      else{ array.append(object) }
   }
}


//MARK: - Helpers
extension MagicsParser{
   private func clearArray<T: MagicsModel>(array: ObservableArray<T>){
      array.forEach { $0.delete() }
      array.array = []
   }
   
   private func insertIndex<T: MagicsModel>(el: T, inArray: [T], startIndex: Int = 0) -> Int{
      if inArray.count == 0 { return startIndex }
      if inArray.count == 1 { return startIndex + (T.isOrderedBefore(el, right: inArray[0]) ? 0 : 1) }
      let split = inArray.split3()
      if T.isOrderedBefore(el, right: split.1){
         return insertIndex(el, inArray: split.0, startIndex: startIndex)
      }else{
         return insertIndex(el, inArray: split.2, startIndex: startIndex + split.0.count + 1)
      }
   }
}
