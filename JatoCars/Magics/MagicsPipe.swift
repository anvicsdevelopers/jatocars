//
//  MagicsPipe.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 11.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation

class MagicsPipe{
   private var tasks = [MagicsTask]()
   private var isRunning = false
   
   private let completeSignal = Signal()
   private let followSignal = EventProducer<Int>()
   private let errorSignal = Signal()
   
   private let API: MagicsAPI
   
   init(API: MagicsAPI){
      self.API = API
   }
   
   init(task: MagicsTask, API: MagicsAPI){
      self.API = API
      tasks.append(task)
   }
   
   func thenInteract<T: MagicsInteractor>(interactor: T) -> MagicsPipe{
      tasks.append(MagicsInteractionTask(interactor: interactor, API: API))
      return self
   }

   func thenInteract<T: MagicsInteractor>(interactorGenerator: () -> T) -> MagicsPipe{
      tasks.append(MagicsInteractionGeneratedTask(interactorGenerator: interactorGenerator, API: API))
      return self
   }

   func thenPerform(block: EmptyBlock) -> MagicsPipe{
      tasks.append(MagicsBlockTask(block: block))
      return self
   }
   
   func thenUpdate<T: MagicsUndependable where T: MagicsInMemory>(array: ObservableArray<T>) -> MagicsPipe{
      let url = T.getURL()
      let task = MagicsInMemoryUpdateArrayTask(array: array, API: API, url: url)
      tasks.append(task)
      return self
   }
   
   func thenUpdate<T: MagicsDependable where T: MagicsInMemory>(array: ObservableArray<T>, root: T.Root) -> MagicsPipe{
      let url = T.getURL(root)
      let task = MagicsInMemoryUpdateArrayTask(array: array, API: API, url: url)
      tasks.append(task)
      return self
   }
   
   func follow() -> EventProducer<Int> { return followSignal }
   func onComplete() -> Signal { return completeSignal }
   func onError() -> Signal { return errorSignal }
   
   func run(){
      guard !isRunning && tasks.count > 0 else { return }
      isRunning = true
      startTaksAt(0)
   }
   
   private func successBlockFor(index: Int) -> EmptyBlock{
      return {
         self.followSignal.next(index)
         self.startTaksAt(index + 1)
      }
   }
   
   private func errorBlock() -> EmptyBlock{
      return {
         self.errorSignal.next()
      }
   }
   
   private func startTaksAt(index: Int){
      API.mprint("MagicsPipe: start task at \(index)")
      guard index < tasks.count else {
         completeSignal.next()
         return
      }
      tasks[index].run(successBlockFor(index), failure: errorBlock())
   }
}
