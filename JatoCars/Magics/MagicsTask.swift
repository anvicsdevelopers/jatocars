//
//  MagicsTask.swift
//  Toyota
//
//  Created by Nikita Arkhipov on 11.07.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import Foundation


protocol MagicsTask{
   func run(success: EmptyBlock, failure: EmptyBlock)
}

class MagicsBlockTask: MagicsTask{
   let block: EmptyBlock
   
   init(block: EmptyBlock){
      self.block = block
   }
   
   func run(success: EmptyBlock, failure: EmptyBlock){
      block()
      success()
   }
}

class MagicsInteractionTask: MagicsTask {
   private let interactor: MagicsInteractor
   private let API: MagicsAPI
   
   init(interactor: MagicsInteractor, API: MagicsAPI){
      self.interactor = interactor
      self.API = API
   }
   
   func run(success: EmptyBlock, failure: EmptyBlock) {
      let fireSignal = Signal()
      fireSignal.observe {
         self.performTask(success, failure: failure)
      }
      interactor.fireWhenReady(fireSignal)
   }
   
   private func performTask(success: EmptyBlock, failure: EmptyBlock) {
      JSONData.getFrom(API.baseURL() + interactor.getURL(), logLevel: API.logLevel(),  modifyRequest: API.configureRequest + interactor.configureRequest) { data, error in
         if self.interactor.interactionIsSuccess(data, error: error){
            self.interactor.processResult(data)
            success()
         }else{
            failure()
         }
      }
   }
}

class MagicsInteractionGeneratedTask: MagicsTask{
   private let interactorGenerator: () -> MagicsInteractor
   private let API: MagicsAPI
   
   init(interactorGenerator: () -> MagicsInteractor, API: MagicsAPI){
      self.interactorGenerator = interactorGenerator
      self.API = API
   }
   
   func run(success: EmptyBlock, failure: EmptyBlock) {
      let interactor = interactorGenerator()
      let fireSignal = Signal()
      fireSignal.observe {
         self.performTask(interactor, success: success, failure: failure)
      }
      interactor.fireWhenReady(fireSignal)
   }
   
   private func performTask(interactor: MagicsInteractor, success: EmptyBlock, failure: EmptyBlock) {
      let url =  API.baseURL() + interactor.getURL()
      JSONData.getFrom(url, logLevel: API.logLevel(), modifyRequest: API.configureRequest + interactor.configureRequest) { data, error in
         if interactor.interactionIsSuccess(data, error: error){
            interactor.processResult(data)
            success()
         }else{
            failure()
         }
      }
   }

}

class MagicsInMemoryUpdateObjectTask <T: MagicsInMemory>: MagicsTask {
   private let model: T
   private let API: MagicsAPI
   private let url: String
   
   init(model: T, API: MagicsAPI, url: String){
      self.model = model
      self.API = API
      self.url = url
   }
   
   func run(success: EmptyBlock, failure: EmptyBlock) {
      API.statusForUpdating(model, url: url).observe { status in
         switch status{
         case .RequestSuccessed: success()
         case .RequestFailed(_): failure()
         default: return
         }
      }
   }
}

class MagicsInMemoryUpdateArrayTask <T: MagicsInMemory>: MagicsTask {
   private let array: ObservableArray<T>
   private let API: MagicsAPI
   private let url: String
   
   init(array: ObservableArray<T>, API: MagicsAPI, url: String){
      self.array = array
      self.API = API
      self.url = url
   }
   
   func run(success: EmptyBlock, failure: EmptyBlock) {
      API.statusForUpdating(array, url: url).observe { status in
         switch status{
         case .RequestSuccessed: success()
         case .RequestFailed(_): failure()
         default: return
         }
      }
   }
}
