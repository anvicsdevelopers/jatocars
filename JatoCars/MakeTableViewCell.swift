//
//  MakeTableViewCell.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 28.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import UIKit

class MakeTableViewCell: UITableViewCell {
   
   var makes = [JatoMake](){
      didSet{ updateMakes() }
   }
   
   let pickedIndex = Observable(0)

   @IBOutlet weak var pickerView: UIPickerView!
      
   func updateMakes(){
      pickerView.delegate = self
      pickerView.dataSource = self
   }
}

extension MakeTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource{
   func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{ return 1 }
   
   func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { return makes.count }

   func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{ return makes[row].makeKey.value }
   
   func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){ pickedIndex.value = row }
}
