//
//  ModelTableViewCell.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 28.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import UIKit

class ModelTableViewCell: UITableViewCell {

   var allModels = [JatoCarModel](){
      didSet{ updateAllModels() }
   }
   
   private var pickedModelIndex = 0{
      didSet{
         let make = makes[pickedModelIndex]
         modelsToShow = allModels.filter { $0.make.value == make }
         pickedModel.value = modelsToShow[0]
      }
   }
   
   private var modelsToShow = [JatoCarModel](){
      didSet{ updateModelsToShow() }
   }
   
   let pickedModel = Observable<JatoCarModel?>(nil)
   
   private var makes = [String]()

   @IBOutlet weak var makePickerView: UIPickerView!
  
   @IBOutlet weak var modelCarPickerView: UIPickerView!
   
   func updateAllModels(){
      makes = allModels.reduce([]) { array, model in array.contains(model.make.value) ? array : array.arrayByAppendingUniq(model.make.value) }
      makePickerView.delegate = self
      makePickerView.dataSource = self
      modelCarPickerView.delegate = self
      modelCarPickerView.dataSource = self
      pickedModelIndex = 0
   }

   func updateModelsToShow(){
      modelCarPickerView.reloadComponent(0)
   }
}

extension ModelTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource{
   func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{ return 1 }
   
   func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      return pickerView == makePickerView ? makes.count : modelsToShow.count
   }
   
   func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
      return pickerView == makePickerView ? makes[row] : modelsToShow[row].title
   }
   
   func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
      if pickerView == makePickerView{
         pickedModelIndex = row
      }else{
         pickedModel.value = modelsToShow[row]
      }
   }
}
