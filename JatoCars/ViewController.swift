//
//  ViewController.swift
//  JatoCars
//
//  Created by Nikita Arkhipov on 28.09.16.
//  Copyright © 2016 Anvics. All rights reserved.
//

import UIKit

//MakeCell
//ModelCell
class ViewController: UIViewController {
   @IBOutlet weak var loadingView: UIView!

   @IBOutlet weak var makesTableView: UITableView!
   @IBOutlet weak var modelsTableView: UITableView!
   
   @IBOutlet weak var loadModelsButton: UIButton!
   @IBOutlet weak var addModelsButton: UIButton!
   @IBOutlet weak var loadCarsButton: UIButton!

   private let makesLoader = JatoMakesLoader()
   
   private let pickedMakesIndexes = ObservableArray<Observable<Int>>()
   private let makesToLoad = ObservableArray<JatoMake>()

   private var modelsLoader: JatoModelsLoader!
   
   private let isLoading = Observable(true)
   
   private let pickedModels = ObservableArray<Observable<JatoCarModel>>()
   
   private var carsLoader: JATOCarLoader!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      modelsLoader = JatoModelsLoader(makes: makesToLoad)
      
      view.bringSubviewToFront(loadingView)
      
      isLoading.map(!).bindTo(loadingView.bnd_hidden)
      
      pickedMakesIndexes.map { $0.sequence.count > 0 }.bindTo(loadModelsButton.bnd_enabled)
      
      pickedMakesIndexes.lift().bindTo(makesTableView) { [unowned self] indexPath, dataSource, tableView in
         let cell = tableView.dequeueReusableCellWithIdentifier("MakeCell") as! MakeTableViewCell
         cell.makes = self.makesLoader.makes.array
         cell.bnd_bag.dispose()
         cell.pickedIndex.bindTo(dataSource[0][indexPath.row]).disposeIn(cell.bnd_bag)
         return cell
      }
      
      modelsLoader.models.map { $0.sequence.count > 0 }.bindTo(addModelsButton.bnd_enabled)
      
      pickedModels.map { $0.sequence.count > 0 }.bindTo(loadCarsButton.bnd_enabled)
      
      pickedModels.lift().bindTo(modelsTableView) { [unowned self] indexPath, dataSource, tableView in
         let cell = tableView.dequeueReusableCellWithIdentifier("ModelCell") as! ModelTableViewCell
         cell.allModels = self.modelsLoader.models.array
         cell.bnd_bag.dispose()
         cell.pickedModel.ignoreNil().bindTo(dataSource[0][indexPath.row]).disposeIn(cell.bnd_bag)
         return cell
      }
      
      let pipe = JATOAPI.sharedAPI.authorize().thenInteract(makesLoader)
      pipe.onComplete().rewrite(false).bindTo(isLoading)
      pipe.run()
   }

   @IBAction func addMake(sender: AnyObject) {
      pickedMakesIndexes.append(Observable(0))
   }
   
   @IBAction func loadModels(sender: AnyObject) {
      makesToLoad.array = pickedMakesIndexes.map { self.makesLoader.makes[$0.value] }
      isLoading.value = true
      let pipe = JATOAPI.sharedAPI.interact(modelsLoader)
      pipe.onComplete().rewrite(false).bindTo(isLoading)
      pipe.run()
   }

   @IBAction func addModel(sender: AnyObject) {
      pickedModels.append(Observable(modelsLoader.models[0]))
   }
   
   
   @IBAction func loadCars(sender: AnyObject) {
      let models = pickedModels.array.map { $0.value }
//      print(models)
      
      let bodiesLoader = JatoBodiesLoader(makes: models)
      carsLoader = JATOCarLoader(bodies: bodiesLoader.bodies)

      isLoading.value = true
      let pipe = JATOAPI.sharedAPI.interact(bodiesLoader).thenInteract(carsLoader)
      pipe.onComplete().rewrite(false).bindTo(isLoading)
      pipe.onComplete().observe {
         self.performSegueWithIdentifier("ShowCars", sender: nil)
      }
      pipe.run()

   }
   
   override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      let destination = segue.destinationViewController as! CarsViewController
      destination.cars = carsLoader.cars
   }
}
